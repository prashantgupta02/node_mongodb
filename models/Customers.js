const mongoose = require('mongoose');
const timestamp = require('mongoose-timestamp');

const CustmorSchema = mongoose.Schema({
    first_name: {
        type: String,
        required: true,
        trim: true
    },
    last_name: {
        type: String,
        required: true,
        trim: true
    },
    Gender: {
        type: String,
        required: true,
        trim: true
    }
});

CustmorSchema.plugin(timestamp);

const Customer = mongoose.model('Customer', CustmorSchema);
module.exports = Customer;